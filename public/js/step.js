$(document).ready(function () {

    init();

    $('#doc-area').on("click", '.add-step', function(e){
        e.preventDefault();
        var next = StepNo();
        var addto = "#step" + (next);

        next = next + 1;

        $(addto).after(function () {
                var step = createStep(next);
                var prev = "#step" + (next-1);
                $(prev).find('.add-step').remove();
            return step;
        });

        //$('#step'+next).attr('data-source',$(addto).attr('data-source'));
    });

    $('#doc-area').on("click", '.remove-step', function (e) {
        e.preventDefault();
        var fieldNum = this.id.charAt(this.id.length - 1);
        var fieldId = "#step" + fieldNum;

        var allSteps        =$('.steps').toArray();
        var beforeLastId    =$(allSteps[allSteps.length-2]).attr('id');
        var beforeLastIdNo  = typeof(beforeLastId) !=='undefined' ? beforeLastId.slice(4, 7) : 1;

        $(this).remove();
        $(fieldId).remove();

        if($('.add-step').length == 0){
            $("#step" + (beforeLastIdNo)).append('<button type="button" class="btn btn-success pull-right add-step">Add Step</button>');
        }

        if($('.steps').length == 0){
                init();
            }
    });

    function init() {
        var next = parseInt(StepNo());
        if(next==1)
            $('#doc-area').html(createStep(next));
    }

    function StepNo() {
            var element = $("div:visible[id^='step']:last").attr('id');
            element = typeof (element) !== 'undefined' ? element : 'step1';
            var res = element.slice(4, 7);
        return parseInt(res);
    }

    function createStep(next) {
        var newSt = '<div id="step'+(next)+'" class="steps"><div class="form-group"><label for="stpTitle'+(next)+'">Step Title</label><input type="text" name="steps[]" class="form-control" id="stpTitle' + (next) + '" aria-describedby="title" placeholder="Enter Step title"></div><div class="form-group"><label for="stpBody' + (next) + '">Step</label><textarea class="form-control" id="stpBody' + (next) + '" name="details[]" rows="8"></textarea></div><button id="remove'+(next)+'" type="button" class="btn btn-danger remove-step">Remove Step</button> <button type="button" class="btn btn-success pull-right add-step">Add Step</button></div>';
        return $(newSt);
    }
});