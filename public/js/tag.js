/**
 * Created by Pondit on 7/8/2017.
 */
$(document).on('click', '.browse', function(){
    var file = $(this).parent().parent().parent().find('.file');
    file.trigger('click');
});

$(document).on('change','.file', function(){
    $(this).parent().find('.form-control').val($(this).val().replace(/c:\\fakepath\\/i, ''));
});

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#image')
                .attr('src', e.target.result)
                .width(275)
                .height(275);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

$('#imgInp').change(function() {
    /* Act on the event */
    readURL(this);
});

