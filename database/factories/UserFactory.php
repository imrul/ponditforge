<?php
/**
 * Created by PhpStorm.
 * User: Pondit
 * Date: 7/11/2017
 * Time: 12:55 PM
 */

    $factory->define(App\User::class, function (Faker\Generator $faker){
        static $password;

        return[
            'name'          => $faker->name,
            'email'         => $faker->unique()->safeEmail,
            'password'      => $password ?: $password = bcrypt('secret'),
            'remember_token'=> str_random(10),
        ];
    });