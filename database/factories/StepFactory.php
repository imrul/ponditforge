<?php
/**
 * Created by PhpStorm.
 * User: Pondit
 * Date: 7/18/2017
 * Time: 11:58 AM
 */

use Carbon\Carbon;

$factory->define(App\Step::class, function (Faker\Generator $faker){


       static $orderId = 1;

        return [
            'post_id'       => $faker->unique()->numberBetween(1, 10),
            'title'         => $faker->sentence(6, true),
            'details'       => $faker->paragraph(5),
            'order'         => $orderId++,
            'published_at'  => Carbon::now()->toDateTimeString(),
            'created_at'    => Carbon::now()->toDateTimeString(),
        ];
    });