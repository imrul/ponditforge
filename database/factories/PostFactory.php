<?php
/**
 * Created by PhpStorm.
 * User: Pondit
 * Date: 7/11/2017
 * Time: 12:55 PM
 */

    use Carbon\Carbon;

    $factory->define(App\Post::class, function(Faker\Generator $faker){

        static $enumVar = ['Y', 'N'];

        return [
            'title'         => $faker->sentence(6, true),
            'slug'          => $faker->slug,
            'excerpt'       => $faker->paragraph(2),
            'body'          => $faker->paragraph(10),
            'publish'       => $enumVar[$faker->numberBetween(0,1)],
            'draft'         => $enumVar[$faker->numberBetween(0,1)],
            'published_at'  => Carbon::now()->toDateTimeString(),
            'created_at'    => Carbon::now()->toDateTimeString(),
        ];
    });