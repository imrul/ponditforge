<?php
/**
 * Created by PhpStorm.
 * User: Pondit
 * Date: 8/1/2017
 * Time: 2:26 PM
 */

    use Carbon\Carbon;
    use App\User;

    $factory->define(App\Comment::class, function(Faker\Generator $faker){
        return [
            'user_id'       => $faker->randomElement(User::pluck('id')->toArray()),
            'comment'       => $faker->sentence(6, true),
            'created_at'    => Carbon::now()->toDateTimeString(),
        ];
    });
