<?php

use Illuminate\Database\Seeder;
use App\Comment;
use App\Post;

class CommentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $posts = Post::all();
        foreach($posts as $post){
            $post->comments()->saveMany(factory(Comment::class, rand(2, 8))->make());
        }
    }
}
