<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Post;
use App\Comment;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(User::class, 10)->create()->each(function ($user){
            $user->posts()->saveMany(factory(Post::class, rand(1,5))->make());
        });
    }
}
