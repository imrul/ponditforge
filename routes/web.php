<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', 'PostController@index');

Route::get('/post/list', 'PostController@showList');

Route::post('/post/search','PostController@search');

Route::get('/tutorial/{post}/{slug?}', 'PostController@show');

Route::resource('post', 'PostController', ['except' => [
    'show'
]]);


Route::group(['prefix'=>'user', 'middleware' => 'auth'], function (){
    Route::resource('comment', 'CommentController');
});


Route::group(['prefix'=>'admin','middleware' => 'auth'], function (){

    Route::get('/home', 'HomeController@index')->name('home');

    Route::post('/tag/search', 'TagController@search');

    Route::resource('tag', 'TagController');

    Route::get('/', 'AdminController@index');
    Route::get('/posts', 'AdminController@posts');
    Route::get('/tags', 'AdminController@tags');
    Route::get('/profile', 'AdminController@profile');
});

