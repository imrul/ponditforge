@extends('layouts.main')
    @section('title', 'Post List')
        @section('content')
            <div class="col-md-8">

                <div class="row">

                    <h2>My Posts</h2>
                    <div class="pull-right">
                        {!! Form::open(['url'=>'/post/list', 'method'=>'get']) !!}
                            <button type="submit" class="btn btn-default btn-info btn-md refresh-btn"><i class="glyphicon glyphicon-refresh"></i>  Refresh</button>
                        {!! Form::close() !!}
                    </div>
                </div>

                <div class="row">
                    <table class="table table-bordered table-striped">
                        <thead >
                            <tr class="bg-info ">
                                <th>Name</th>
                                <th>Email</th>
                                <th>Title</th>
                                <th>Operation</th>
                            </tr>
                        </thead>
                        <tbody id="list-items">
                            @foreach($posts as $post)
                                <tr>
                                    <td>{{$post->user->name}}</td>
                                    <td>{{$post->user->email}}</td>
                                    <td>{{$post->title}}</td>

                                    <td id="icon-crud" style="width:180px; text-align: center">
                                        <a href="{{url('/tutorial/'.$post->id.'/'.$post->slug)}}" class="btn btn-sm btn-default"><i class="icon-trash glyphicon glyphicon-eye-open text-primary"></i></a>

                                        <a href="{{'/post/'.$post->id.'/edit'}}"><i class="icon-trash glyphicon glyphicon-edit text-primary"></i></a>

                                        {!! Form::open(['url' => '/post/'.$post->id, 'method' => 'delete']) !!}
                                        {{csrf_field()}}
                                        <button type="submit" class="btn btn-sm btn-default"><i class="icon-trash glyphicon glyphicon-trash text-danger"></i></button>
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{$posts->links()}}
                </div>
            </div>
            @include('partial.sidebar')
        @endsection