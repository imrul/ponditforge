@extends('layouts.main')

    @section('title', 'Home-Ponditforge')

        @section('content')

            <!-- Blog Entries Column -->
            <div class="col-md-8">
                <h1 class="page-header">
                   Page Heading
                   <small>Something</small>
                </h1>
                        <!--Blog Post -->
                        @foreach($posts as $post)
                                <h2>
                                    <a href="{{url('/tutorial/'.$post->id.'/'.$post->slug)}}">{{$post->title}}</a>
                                </h2>
                                <p class="lead">
                                    by <a href="#">{{$post->user->name}}</a>
                                </p>
                                <p><span class="glyphicon glyphicon-time"></span> Posted on {{$post->created_at->diffForHumans()}}</p>
                                <hr>
                                <img class="img-responsive" src="http://placehold.it/900x300" alt="">
                                <hr>
                                <p>{{$post->excerpt}}</p>
                                <a class="btn btn-primary" href="{{url('/tutorial/'.$post->id.'/'.$post->slug)}}">Read More <span class="glyphicon glyphicon-chevron-right"></span></a>
                                <hr>
                        @endforeach
                            {{$posts->links()}}
                        {{--<!-- Pager -->
                        <ul class="pager">
                            <li class="previous">
                                <a href="#">&larr; Older</a>
                            </li>
                            <li class="next">
                                <a href="#">Newer &rarr;</a>
                            </li>
                        </ul>--}}

            </div>

            @include('partial.sidebar')

        @stop

