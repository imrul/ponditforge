    @extends('layouts.main')
        @section('title', 'Edit Post')

        @section('content')
            <h3> Create New Post</h3>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="col-md-8">
                <form action="{{url('/post/'.$post->id)}}" method="post">

                    {{csrf_field()}}
                    {{method_field('PUT')}}

                    <div class="form-group">
                        <label for="postTitle">Title</label>
                        <input type="text" name="title" class="form-control" id="postTitle" aria-describedby="title" placeholder="Enter title" value="{{$post->title}}">
                        <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                    </div>

                    <div class="form-group">
                        <label for="postSummary">Summary</label>
                        <textarea class="form-control" name="excerpt" id="postSummary" rows="6">{{$post->excerpt}}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="postBody">Details</label>
                        <textarea class="form-control" name="body" id="postBody" rows="10">{{$post->body}}</textarea>
                    </div>

                    <div id="doc-area">
                        @foreach($post->steps as $step)
                            <div id="{{'step'.$step->order}}" class="steps">
                                <div class="form-group">
                                    <label for="{{'stepTitle'.$step->order}}">Step Title</label>
                                    <input type="text" name="steps[]" class="form-control" id="{{'stepTitle'.$step->order}}" aria-describedby="title" placeholder="Enter Step title" value="{{$step->title}}">
                                </div>

                                <div class="form-group">
                                    <label for="{{'stepBody'.$step->order}}">Step</label>
                                    <textarea class="form-control" id="{{'stepBody'.$step->order}}" name="details[]" rows="15">{{$step->details}}</textarea>
                                </div>
                                <button id="{{'remove'.$step->order}}" type="button" class="btn btn-danger remove-step">Remove Step</button>
                                <button type="button" class="btn btn-success pull-right add-step">Add Step</button>
                            </div>
                        @endforeach
                    </div>

                    <div class="input-group radio-status">
                        <div class="status">
                            <label for="publish"><input type="radio" name="status" value="Y" @if($post->publish == 'Y') {{'checked="checked"'}} @endif>Publish</label>
                        </div>
                        <div class="status">
                            <label for="draft"><input type="radio" name="status" value="N" @if($post->publish == 'N') {{'checked="checked"'}} @endif>Draft</label>
                        </div>
                    </div>

                    <br><br>
                    <button type="submit" class="btn btn-primary btn-block">Submit</button>
                </form>
            </div>
            @include('partial.sidebar')
        @endsection