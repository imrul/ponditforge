@extends('layouts.main')

    @section('title', 'Post-Ponditforge')

        @section('content')

            <!-- Blog Post Content Column -->
            <div class="col-lg-8">

                <!-- Blog Post -->

                <!-- Title -->
                <h1>{{$post->title}}</h1>

                <!-- Author -->
                <p class="lead">
                    by <a href="#">{{$post->user->name}}</a>
                </p>

                <hr>

                <!-- Date/Time -->
                <p><span class="glyphicon glyphicon-time"></span>{{$post->created_at->diffForHumans()}}</p>

                <hr>

                <!-- Preview Image -->
                <img class="img-responsive" src="http://placehold.it/900x300" alt="">

                <hr>

                <!-- Post Content -->
                <p class="lead">
                    {{$post->body}}
                </p>

                @foreach($post->steps as $step)
                    <h2 style="color:#314D7C; font-weight: 800; font-size: 21px">{{$step->title}}</h2>
                    <hr style="border-top: 1px solid #8c8b8b;">
                    <p class="lead">
                        {{$step->details}}
                    </p>
                    <br>
                @endforeach

                <hr>
                <!-- Blog Comments -->
                <!-- Comments Form -->
                <div class="well">
                    <h4>Leave a Comment:</h4>
                    @if(Auth::check())
                    {!! Form::open(['url' =>'/user/comment/', 'method' => 'post', 'role'=>'form']) !!}
                        {{csrf_field()}}
                        <div class="form-group">
                            {!! Form::textarea('comment',null, ['class'=>'form-control', 'rows'=>'3']) !!}
                            {!! Form::hidden('post-id', $post->id) !!}
                        </div>
                        {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
                    {!! Form::close() !!}
                    @else
                        <h5>You are not logged in. Please Login or complete the registration</h5>
                        {!! link_to('/login', 'Login', ['class' => 'btn btn-success']) !!}
                        {!! link_to('/register', 'Register', ['class' => 'btn btn-primary pull-right']) !!}
                    @endif
                </div>

                <hr>

                <!-- Posted Comments -->

                <!-- Comments -->
                @foreach($post->comments as $comment)
                <div class="media">
                    <a class="pull-left" href="#">
                        <img class="media-object" src="http://placehold.it/64x64" alt="">
                    </a>
                    <div class="media-body">
                        <h4 class="media-heading">{{$comment->user->name}}
                            <small>{{$comment->created_at->diffForHumans()}}</small>
                        </h4>
                        {{$comment->comment}}
                    </div>
                </div>
                @endforeach
                <!-- Comment -->
            </div>

            @include('partial.sidebar')

        @stop