@extends('layouts.main')

    @section('title','Create-Ponditforge')
            @section('content')
                <h3> Create New Post</h3>

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="col-md-8">
                    <form action="{{url('/post')}}" method="post">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="postTitle">Title</label>
                            <input type="text" name="title" class="form-control" id="postTitle" aria-describedby="title" placeholder="Enter title">
                            <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                        </div>

                        <div class="form-group">
                            <label for="postSummary">Summary</label>
                            <textarea class="form-control" name="excerpt" id="postSummary" rows="5"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="postBody">Details</label>
                            <textarea class="form-control" name="body" id="postBody" rows="6"></textarea>
                        </div>

                        <div id="doc-area">
                            <!--step field injected from jQuery-->
                        </div>

                        <div class="input-group radio-status">
                            <div class="status">
                                <label for="publish"><input type="radio" name="status" value="Y">Publish</label>
                            </div>
                            <div class="status">
                                <label for="draft"><input type="radio" name="status" value="N">Draft</label>
                            </div>
                        </div>
                        <br><br>
                        <button type="submit" class="btn btn-primary btn-block">Submit</button>
                    </form>
                </div>
        @include('partial.sidebar')
    @stop