@extends('layouts.admin.master')
    @section('title', 'My Post')
        @section('post','current')

    @section('content')
        <div class="content-box-large">
            <div class="panel-heading">
                <div class="panel-title">All Post</div>

                <div class="panel-options">
                    <a href="{{url('/admin/posts')}}" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
                    <a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-hover">
                    <thead>
                     <tr>
                         <th>#</th>
                         <th>Title</th>
                         <th>Status</th>
                         <th>Date</th>
                         <th>Operation</th>
                     </tr>
                    </thead>
                    <tbody>
                    @foreach($posts as $post)
                     <tr>
                        <td>{{$post->id}}</td>
                        <td><a href="{{url('/tutorial/'.$post->id.'/'.$post->slug)}}">{{$post->title}}</a></td>
                        <td>@if($post->publish == 'Y'){{'Published'}} @elseif($post->draft == 'Y') {{'Drafted'}} @endif</td>
                        <td>{{$post->published_at}}</td>

                        <td id="icon-crud" style="width:180px; text-align: center">
                            <a href="{{url('/tutorial/'.$post->id.'/'.$post->slug)}}" class="btn btn-sm btn-default"><i class="icon-trash glyphicon glyphicon-eye-open text-primary"></i></a>
                            <a href="{{url('/post/'.$post->id.'/edit')}}"><i class="icon-trash glyphicon glyphicon-edit text-primary"></i></a>
                            {!! Form::open(['url' => '/post/'.$post->id, 'method' => 'delete']) !!}
                                {{csrf_field()}}
                            <button type="submit" class="btn btn-sm btn-default"><i class="icon-trash glyphicon glyphicon-trash text-danger"></i></button>
                            {!! Form::close() !!}
                        </td>
                     </tr>
                    @endforeach
                    </tbody>
                </table>
                {{$posts->links()}}
            </div>
        </div>
    @endsection