<!DOCTYPE html>
<html lang="en">
    <!-- head start-->
        @include('layouts.admin.partials.head')
    <!-- head end -->

    <body>
        <!------ start of header ------>
            @include('layouts.admin.partials.header')
        <!------ end of header -------->

        <div class="page-content">
            <div class="row">
                <div class="col-md-2">
                    <!-- start of sidebar -->
                        @include('layouts.admin.partials.sidebar')
                    <!--end of sidebar -->
                </div>
                <div class="col-md-10">

                    <!--start of content -->
                        @yield('content')
                    <!--end of content   -->

                </div>
            </div>
        </div>

        <!--footer start -->
            @include('layouts.admin.partials.footer')
        <!--footer end -->

    </body>
</html>
