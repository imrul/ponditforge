<div class="sidebar content-box" style="display: block;">
    <ul class="nav">
        <!-- Main menu -->
        <li class="@yield('dash')"><a href="{{url('/admin')}}"><i class="glyphicon glyphicon-home"></i> Dashboard</a></li>
        <li class="@yield('post')"><a href="{{url('/admin/posts')}}"><i class="glyphicon glyphicon-calendar"></i> My Posts</a></li>
        <li class="@yield('tag')"><a href="{{url('/admin/tags')}}"><i class="glyphicon glyphicon-stats"></i> Tags</a></li>
        <li class="@yield('profile')"><a href="{{url('/admin/profile')}}"><i class="glyphicon glyphicon-user"></i> Profile</a></li>

        <li class="submenu">
            <a href="#">
                <i class="glyphicon glyphicon-list"></i> Pages
                <span class="caret pull-right"></span>
            </a>
            <!-- Sub menu -->
            <ul>
                <li><a href="login.html">Login</a></li>
                <li><a href="signup.html">Signup</a></li>
            </ul>
        </li>
    </ul>
</div>