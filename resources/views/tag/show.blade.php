@extends('layouts.main')
    @section('title', 'Show Tag')

        @section('content')
            <div class="col-md-8" style="margin-top: 50px">
                <div class="col-md-6">
                    <img src="{{asset('/photos').'/'.$item->image}}" class="img-thumbnail" alt="{{$item->image}}">
                </div>
                <div class="col-md-6 pull-right">
                    <table class="table">
                        <tr>
                            <td>Tag:</td>
                            <td>{{$item->name}}</td>
                        </tr>
                        <tr>
                            <td>Status:</td>
                            @if($item->status ==='Y')
                                <td>Published</td>
                            @else
                                <td>Drafted</td>
                            @endif
                        </tr>
                    </table>
                    <ul class="list-inline pull-right">
                        <li class="list-inline-item">
                            <a href="{{'/tag/'.$item->id.'/edit'}}"><button type="button" class="btn btn-info">Edit</button></a>
                        </li>
                        <li class="list-inline-item">
                            <form class="form-group" action="{{'/tag/'.$item->id}}" method="post">
                                {{csrf_field()}}
                                {{method_field('DELETE')}}
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
            @include('partial.sidebar')
        @stop
