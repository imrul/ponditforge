@extends('layouts.main')
@section('title', 'Tag-Ponditforge')

@section('content')

    <h3>Edit Tag</h3>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="col-md-8">
        <form enctype="multipart/form-data" action="{{'/tag/'.$item->id}}" method="post">
            {{csrf_field()}}
            {{method_field('PUT')}}
            <div class="form-group">
                <label for="postName">Tag</label>
                <input type="text" name="name" value="{{$item->name}}" class="form-control" id="postName" aria-describedby="tag" placeholder="Enter Tag">
                <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
            </div>

            <div class="row">
                <div class="col-md-6">
                    <input type="file" name="image" value="{{$item->image}}" class="file" id="imgInp">
                    <div class="input-group">
                        <img id="image" src="{{url('photos/'.$item->image)}}" alt="Upload Image" height="275" width="275">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="input-group radio-status">
                        <div class="status">
                            <label for="publish"><input type="radio" name="status" value="Y" @if($item->status == 'Y') {{'checked="checked"'}} @endif>Publish</label>
                        </div>
                        <div class="status">
                            <label for="draft"><input type="radio" name="status" value="N" @if($item->status == 'N') {{'checked="checked"'}} @endif>Draft</label>
                        </div>
                    </div>
                    <div class="input-group browse-button">
                        <button class="browse btn btn-primary input" type="button"><i class="glyphicon glyphicon-search"></i> Browse</button>
                    </div>
                </div>
            </div>
            <hr>
            <div class="form-group sbt-button">
                <button type="submit" class="btn btn-primary center-block">Submit</button>
            </div>
        </form>
    </div>
    @include('partial.sidebar')
@stop