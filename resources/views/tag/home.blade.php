@extends('layouts.main')
    @section('title', 'Image Show')

        @section('content')

            <div class="col-md-8">

                <div class="row">

                    <h2>Tag List</h2>
                        @if(Session::has('flash_message'))
                            <div class="alert alert-success {{ Session::has('flash_message_important') ? 'alert-important' : '' }}">

                                @if(Session::has('flash_message_important'))
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                @endif

                                {{Session::get('flash_message')}}
                            </div>
                        @endif

                    <ul class="list-group col-md-8">
                        @foreach($tags as $tag)
                            <li class="list-group-item">
                                <a href="{{'/tag/'.$tag->id}}">{{$tag->name}}</a>
                                <span class="pull-right">{{$tag->created_at->diffForHumans()}}</span>
                            </li>
                        @endforeach
                    </ul>

                    <ul class="list-group col-md-4">
                        @foreach($tags as $tag)
                            <li class="list-group-item">
                                <a href="{{'/tag/'.$tag->id.'/edit'}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                <form class="form-group pull-right" action="{{'/tag/'.$tag->id}}" method="post">
                                    {{csrf_field()}}
                                    {{method_field('DELETE')}}
                                    <button type="submit" style="border: none"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                </form>
                            </li>
                        @endforeach
                    </ul>
                </div>

                <div class="row">
                    <div class="col-md-2 col-md-offset-10">
                        <a href="/tag/create"><button type="button" class="btn btn-info">Add New</button></a>
                    </div>
                </div>

                {{$tags->links()}}
            </div>

        @include('partial.sidebar')
    @stop