@extends('layouts.main')
    @section('title','Search')
        @section('content')

            <div class="col-md-8">
                <hgroup class="mb20">
                    <h1>Search Results</h1>
                    <h2 class="lead"><strong class="text-danger">{{$items}}</strong> results were found for the search for <strong class="text-danger">{{$keyword}}</strong></h2>
                </hgroup>

                @foreach($results as $result)
                    <section class="col-xs-12 col-sm-6 col-md-12">
                        <article class="search-result row">
                            <div class="col-xs-12 col-sm-12 col-md-3">
                                <a href="#" title="Lorem ipsum" class="thumbnail"><img src="{{asset('/photos').'/'.$result->image}}" alt="Lorem ipsum" /></a>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-2">
                                <ul class="meta-search">
                                    <li><i class="glyphicon glyphicon-calendar"></i> <span>{{$result->created_at}}</span></li>
                                    {{--<li><i class="glyphicon glyphicon-time"></i> <span>4:23 P.M.</span></li>--}}
                                </ul>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-7 excerpet">
                                <h3><a href="{{url('/tag/'.$result->id)}}" title="">{{$result->name}}</a></h3>
                                <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem, exercitationem, suscipit, distinctio, qui sapiente aspernatur molestiae non corporis magni sit sequi iusto debitis delectus doloremque.</p> -->
                                <!-- <span class="plus"><a href="#" title="Lorem ipsum"><i class="glyphicon glyphicon-plus"></i></a></span> -->
                            </div>
                            <span class="clearfix borda"></span>
                        </article>
                    </section>
                @endforeach
            </div>

            @include('partial.sidebar')

        @endsection