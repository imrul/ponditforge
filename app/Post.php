<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Post extends Model
{
    //using soft deleting for retrieving again.
    use SoftDeletes;

    protected $fillable=[
        'author_id','title','slug', 'excerpt', 'body','publish','draft','published_at'
    ];

    protected $attributes=[
        'published_at' => self::CREATED_AT,
    ];

    public function setTitleAttribute($title)
    {
        $this->attributes['title'] = $title;
        $this->attributes['slug'] = str_slug($title, '-');
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function($post)
        {
            if (Auth::check()) {
                $post->author_id = Auth::user()->id;
            }
        });
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'author_id');
    }

    public function steps()
    {
        return $this->hasMany(Step::class, 'post_id');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class, 'post_id');
    }


}
