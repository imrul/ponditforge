<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Step extends Model
{
    //
    protected $fillable = [
                'post_id', 'title', 'details', 'order'
    ];

    public function post()
    {
        return $this->belongsTo(Post::class, 'post_id');
    }
}
