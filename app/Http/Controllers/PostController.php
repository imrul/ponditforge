<?php

namespace App\Http\Controllers;


use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Database\QueryException as Exception;
use App\Post;
use App\Step;


class PostController extends Controller
{

    private $searchField = 'post';

    public function __construct()
    {
        $this->middleware('auth', ['only' => ['create', 'store', 'edit', 'update', 'destroy', 'showList']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $field = $this->searchField;

        $posts = Post::where('publish', 'Y')->orderBy('created_at', 'desc')->paginate(5);

        return view('blog.home', compact('posts', 'field'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $field = $this->searchField;

        return view('blog.create', compact('field'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'title' => 'required',
            'excerpt' => 'required',
            'body' => 'required',
        ]);



        $data = $request->all();

        static $order = 0;
        $steps = $request->input('steps');
        $details = $request->input('details');
        $stepData = [];
        foreach ($steps as $step) {
            $stepData[] = new Step([
                'title' => $step,
                'details' => $details[$order],
                'order' => ++$order,
            ]);
        }

        try {

            Post::create($data)->steps()->saveMany($stepData);
            Session::flash('flash_message', 'Post Created Successfully!');

        } catch (Exception $e) {
            return redirect()->back()
                ->withErrors($e->getMessage())
                ->withInput();
        }

        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post, $slug = null)
    {
        /*$post = Post::findOrFail($id);*/

        $field = $this->searchField;

        if(empty($slug)){
            return redirect('/tutorial/'.$post->id.'/'.$post->slug, 301);
        }
        //dd($post);
        return view('blog.post', compact('post', 'field'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
        $field = $this->searchField;

//        $post = Post::findOrFail($id);

        return view('blog.edit', compact('post', 'field'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        //$post = $request->all();

        $this->validate($request, [
            'title' => 'required',
            'excerpt' => 'required',
            'body' => 'required',
        ]);

        $data = $request->all();

        static $order = 0;
        $steps = $request->input('steps');
        $details = $request->input('details');

        $stepData = [];
        foreach ($steps as $step) {
            $stepData[] = new Step([
                'title' => $step,
                'details' => $details[$order],
                'order' => ++$order,
            ]);
        }

        //dd($data);
        try {

            $post = Post::findOrFail($id);

            $post->update($data);
            $post->steps()->delete();
            $post->steps()->saveMany($stepData);

            Session::flash('flash_message', 'Post Updated Successfully!');

        } catch (Exception $e) {
            return redirect()->back()
                ->withErrors($e->getMessage())
                ->withInput();
        }

        return redirect('/post/list');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Post::findOrFail($id);
        $item->delete();
        Session::flash('flash_message', 'Post has been deleted.');
        return back();
    }

    public function showList()
    {
        $field = $this->searchField;

        $userId = Auth::user()->id;
        $posts = Post::where('author_id', '=', $userId)->paginate(5);
        //dd($post);
        return view('blog.list', compact('posts', 'field'));
    }

    public function search(Request $request)
    {
        $field = $this->searchField;

        $keyWord = trim(strtolower($request->input('search')));

        if(!empty($keyWord)){

            $results = Post::where('title', 'like', '%'.$keyWord.'%')->orWhere('excerpt','like','%'.$keyWord.'%')->orWhere('body', 'like', '%'.$keyWord.'%')->get();
            $items = $results->count();

            $inputKey = $request->input('search');
            return view('blog.search', compact('field'))->with('results', $results)->with('items', $items)->with('keyword', $inputKey);
        }else{
            return view('blog.error', compact('field'))->with('error','Search Key must not be empty.');
        }
    }

}
