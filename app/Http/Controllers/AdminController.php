<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\post;


class AdminController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
       return view('admin.dashboard');
    }

    public function posts()
    {
        $userId = Auth::user()->id;
        $posts = Post::where('author_id', '=', $userId)->paginate(10);
        return view('admin.posts', compact('posts'));
    }

    public function tags()
    {
        return view('admin.tags');
    }

    public function profile()
    {
        return view('admin.profile');
    }
}
