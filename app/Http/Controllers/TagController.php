<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Database\QueryException as Exception;
use App\Tag;

class TagController extends Controller
{
    //
    private $searchField = 'tag';

    public function index()
    {
        $field = $this->searchField;

        $tags = Tag::paginate(3);
        return view('tag.home', compact('tags', 'field'));
    }




    public function create()
    {
        $field = $this->searchField;
        return view('tag.create', compact('field'));
    }




    public function show($id)
    {
        $field = $this->searchField;

        $item = Tag::findOrFail($id);

        return view('tag.show', compact('item', 'field'));
    }



    public function store(Request $request)
    {
        $this->validate($request,[
            'name'  => 'required|unique:tags',
        ]);

        if ($request->hasFile('image')) {

            $imgName = time().'.'.$request->file('image')->getClientOriginalExtension();
            $request->image->move(public_path('photos'), $imgName);

        } else {
            $imgName = "noimage.png";
        }

        $status = $request->input('status');
        if(!isset($status)){
            $status = 'Y';
        }

        $data = [
            'name' => $request->input('name'),
            'image' => $imgName,
            'status' => $status,
        ];

        try {
            Tag::create($data);
            Session::flash('flash_message', 'Tag Created Successfully!');

        } catch (Exception $e) {
            return redirect()->back()
                ->withErrors($e->getMessage())
                ->withInput();
        }

        return redirect('/tag');
    }





    public function edit($id)
    {
        $field = $this->searchField;

        $item = Tag::find($id);
        return view('tag.edit', compact('item', 'field'));
    }




    public function update(Request $request, $id)
    {
       $this->validate($request,[
            'name'  => 'required|unique:tags,name,'.$id,
        ]);

        if ($request->hasFile('image')) {

            $imgName = time().'.'.$request->file('image')->getClientOriginalExtension();
            $request->image->move(public_path('photos'), $imgName);

        } else {
            $imgName = Tag::findOrFail($id)->image;
        }

        $data = [

            'name' => $request->input('name'),
            'image' => $imgName,
            'status' => $request->input('status'),
        ];

        try {

            Tag::findOrFail($id)->update($data);
            Session::flash('flash_message', 'Tag Updated successfully.');

        } catch (Exception $e) {
            return redirect()->back()
                ->withErrors($e->getMessage())
                ->withInput();
        }
        return redirect('/tag');
    }





    public function destroy($id)
    {

        $item = Tag::find($id);
        $imageFile = public_path().'/photos/'.$item->image;

        if(strcmp($item->image, 'noimage.png')){

            \File::delete($imageFile);
        }

        $item->delete();
        Session::flash('flash_message', 'Tag has been deleted.');
        return back();
    }




    public function search(Request $request)
    {
        $field = $this->searchField;

        $keyWord = trim(strtolower($request->input('search')));

        if(!empty($keyWord)){

            $results = Tag::where('name', $keyWord)->orWhere('name', 'like', '%'.$keyWord.'%')->get();
            $items = $results->count();

            $inputKey = $request->input('search');
            return view('tag.search', compact('field'))->with('results', $results)->with('items', $items)->with('keyword', $inputKey);
        }else{
            return view('tag.error', compact('field'))->with('error','Search Key must not be empty.');
        }
    }


}
